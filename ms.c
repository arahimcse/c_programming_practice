#include<stdio.h>
int isMountain(int A[], int length)
{
	int flag =0;
	printf("Peak[");
	for(int i = 1; i<length; i++)
	{
		if (A[i-1]<A[i] && A[i]>A[i+1])
		{
			if(flag == 0)
			{
				printf("%d", A[i]);
			} else
			{
				printf(", %d", A[i]);
			}
			flag++;
		}
	}
	return flag;
}
int main()
{
	int n, results;
	printf("Enter the length of list: ");
	scanf("%d", &n);
	int A[n];
	printf("Enter the list\n");
	for(int i=0; i<n; i++)
	{
		scanf("%d", &A[i]);
	}
	results = isMountain(A, n);
	if(results == 1)
	{
		printf("] = %d. It is Mountain Sequence", results);
	} else {
		printf("] = %d. It is not Mountain Sequence",  results);
	}
	return 0;
}