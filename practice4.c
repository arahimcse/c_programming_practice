#include<stdio.h>
int sumDigits(int num);

int main()
{
	int n;
	printf("Input: ");
	scanf("%d", &n);
	printf("Output: %d\n", sumDigits(n));
	return 0;
}

int sumDigits(int num)
{
	if(num != 0)
	{
		return num%10 + sumDigits(num/10);
	}
	else
	{
		return 0; 
	}
}