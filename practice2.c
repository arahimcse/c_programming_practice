/*
write a program that wil check a IP address class
example:
input: 192.111.203.1
outpur: class c
*/

#include<stdio.h>
#include<string.h>

int main()
{
	char st[100];
	printf("Input: ");
	gets(st);
	int temp, i, sum;
	temp = 0;
	sum = 0;
	i =0;
	while(st[i] != '.')
	{
		temp = st[i] - 48;
		sum = sum*10 + temp;
		i++;
	}
	printf("Output: ");
	if(sum <= 255)
	{
		if(sum <=126)
		{
			printf("Class A");
		} 
		else if( sum == 127)
		{
			printf("Loop Back address");
		} 
		else if( sum <= 191)
		{
			printf("class B");
		}
		else if( sum <= 223)
		{
			printf("Class C");
		}
		else if( sum <= 239)
		{
			printf("Class D");
		}
		else
		{
			printf("Class E");
		}

	} else
	{
		printf("Not Valid! IPV4 valid range 0 to 255");
	}
	return 0;
}