import java.util.*;
class practice9{
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		System.out.println("Enter the length of list ");
		int n;
		n = input.nextInt();
		System.out.println("Enter the list");
		int[] arr;
		arr = new int[n];
		for(int i=0; i<n; i++)
		{
			arr[i] = input.nextInt();
		}
		bubbleSort(arr, n);
		System.out.println("Ascending order of list ");
		for(int i =0; i<n; i++)
		{
			System.out.print(arr[i] + " ");
		}
	}
	private static void bubbleSort(int[] arr, int n)
	{
		int temp;
		for(int i=0; i<n; i++)
		{
			for(int j = 0; j<n-i-1; j++)
			{
				if(arr[j] >= arr[j+1])
				{
					temp = arr[j];
					arr[j] = arr[j+1];
					arr[j+1] = temp;
				}
			}
		}
	}
}