#include<stdio.h>
#include<stdlib.h>

int main()
{
	FILE *fi;
	char path[40], ch, pre;
	int c, l,w, wsc;
	printf("Enter the path of file");
	scanf("%s", path);
	fi = fopen(path, "r");
	c = l = w = wsc = 0;
	if(fi == NULL)
	{
		printf("Unable of Open\n");
		printf("Check the file source and privilege");
		exit(EXIT_FAILURE);
	} 
	pre ='\0';
	while((ch = fgetc(fi)) != EOF)
	{
		//characters count
		if(ch != ' ' && ch != '\t' && ch != '\n' && ch != '\0'){c++;}
		if(ch == ' ' && ch != '\t' && ch != '\n' && ch != '\0') {wsc++;}
		
		/*words count*/
		if(ch == ' ' || ch == '\t' || ch == '\n' || ch == '\0')
		{
			if(pre != ' ' && pre != '\t' && pre != '\n' && pre != '\0')
			{
				w++;
			}
		}
		pre = ch;
		
		//line counts
		if (ch == '\n')
            l++;
		
	}
	if (c > 0)
    {
        l++;
    }

	//Display of results
    printf("\n");
    printf("Total characters without space = %d and with space = %d\n", c, c+wsc);
    printf("Total words = %d\n", w);
    printf("Total lines      = %d\n", l);
	
	fclose(fi);
	return 0;
}

