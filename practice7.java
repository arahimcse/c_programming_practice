import java.util.*;
class practice7{
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		System.out.print("Enter the length of array ");
		int n;
		n = input.nextInt();
		System.out.println("Enter the list of array");
		int[] arr;
		arr = new int[n];
		for(int i = 0; i<n; i++)
		{
			arr[i] = input.nextInt();
		}
		quickSort(arr, 0, n-1);
		for(int i=0; i<n; i++)
		{
			System.out.print(arr[i] +" ");
		}
	}
	private static void quickSort(int[] arr, int l, int h)
	{

		if(l<h)
		{
			int pi;
			pi = partitionOfList(arr, l, h);
			quickSort(arr, l, pi-1);
			quickSort(arr, pi+1, h);	
		}
	}
	private static int partitionOfList(int[] arr, int low, int high)
	{
		int pivot, i, temp;
		pivot = arr[high];
		i = low - 1;
		for(int j = low; j<high; j++)
		{
			if(arr[j] <= pivot)
			{
				i++;
				temp = arr[i];
				arr[i] = arr[j];
				arr[j] = temp;
			}
		}
		temp = arr[i+1];
		arr[i+1] = arr[high];
		arr[high] = temp;
		return i+1;
	}
}