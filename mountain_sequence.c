#include<stdio.h>
int isMountain(int A[], int length)
{
	int flag =0;
	printf("Peak[");
	for(int i=1; i<length-1; i++)
	{
		if( A[i-1]<A[i] && A[i]>A[i+1])
		{
			if(flag == 0 )
			{
				printf("%d", A[i]);
			} else
			{
				printf(", %d", A[i]);
			}
			flag++;
		}
	}
	return flag;
}
int main()
{
	int n, results;
	printf("Enter the length of list\n");
	scanf("%d", &n);
	int arr[n];
	if(n <= 2 )
	{
		printf("Need 3 or more items");
		return 0;
	}
	printf("Enter the list\n");
	for(int i =0; i<n; i++)
	{
		scanf("%d", &arr[i]);
	}
	results = isMountain(arr, n);
	if(results == 1)
	{
	printf("] = %d. It is mountain sequence", results);
	} else
	{
		printf("]= %d. It's not mountain sequence", results);
	}
	return 0;
}

/*
Output
Enter the length of list
7
Enter the list
1 2 3 4 3 2 1
Peak[4] = 1. It is mountain sequence

Enter the length of list
8
Enter the list
1 2 1 0 3 4 3 2
Peak[2, 4]= 2. It's not mountain sequence

*/