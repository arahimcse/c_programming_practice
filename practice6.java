import java.util.*;
class practice6{
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		System.out.print("Enter the length "); 
		int n;
		n = input.nextInt();
		int[] arr;
		arr = new int[n];
		System.out.print("Entert the list\n");
		for(int i =0; i<n; i++)
		{
			arr[i] = input.nextInt();
		}
		/*
		insertion sort
		*/

		insertionSort(arr, n);

		/*
		print the sorted data
		*/
		for(int i =0; i<n; i++)
		{
			System.out.print(arr[i]+" ");
		}

	}

	private static void insertionSort(int[] a, int n)
	{
		for(int i =1; i<n; i++)
		{
			int j, max;
			max = a[i];
			j = i - 1;
			while(j>=0 && max<a[j])
			{
				a[j+1] = a[j];
				j--;
			}
			a[j+1] = max;
		}
	}
}