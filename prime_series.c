#include<stdio.h>
void primeSeries(int s, int e)
{
	int i, j;
	for(i = s; i<=e; i++)
	{
		for(j = 2; j<=i; j++)
		{
			if(i%j == 0)
			{
				break;
			}
		}
		if(i == j)
		{
			printf("%d ", i);
		}
	}
}

int main()
{
	int s, e;
	printf("Enter the Starting Numbers\n");
	scanf("%d", &s);
	printf("Enter the Ending Numbers\n");
	scanf("%d", &e);
	primeSeries(s, e);
	return 0;
}