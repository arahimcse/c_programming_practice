#include<stdio.h>
#include<string.h>
int main()
{
	char st[400];
	printf("Input: ");
	gets(st);
	int pre, i, count;
	i =0;
	count =0;
	pre = '\0';
	while(1)
	{
		// check white space, tab, new line and null characters
		if(st[i] == ' ' || st[i] == '\t' || st[i] == '\n' || st[i] == '\0')
		{
			// count words
			if(pre != ' ' && pre != '\t' && pre != '\n' && pre != '\0')
			{
				count++;
			}
		}
		//assign previous character
		pre = st[i];

		//terminal or continue until end the string
		if (st[i] == '\0')
		{
			break;
		} else
		{
			i++;
		}
	}
	printf("Output: %d", count);
	return 0;
}