import java.util.*;
class practice8{
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		System.out.println("Length of list");
		int n;
		n = input.nextInt();
		System.out.println("Enter the list");
		int[] arr;
		arr = new int[n];
		for(int i = 0; i<n; i++)
		{
			arr[i] = input.nextInt();
		}
		selectionSortt(arr, n);
		System.out.println("Ascending order of given list");
		for(int i=0; i<n; i++)
		{
			System.out.print(arr[i]+" ");
		}
	}
	private static void selectionSortt(int[] arr, int n)
	{
		int temp;
		for(int i=0; i<n; i++)
		{
			for(int j=i+1; j<n; j++)
			{
				if(arr[i] >= arr[j])
				{
					temp = arr[i];
					arr[i] = arr[j];
					arr[j] = temp;
				}
			}
		}
	}
}