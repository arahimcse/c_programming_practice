import java.util.*;
class practice5{
	public static void main(String[] args) {
		String st;
		Scanner input = new Scanner(System.in);
		System.out.print("Input: ");
		st = input.nextLine();
		if(isValid(st))
		{
			System.out.println("Balanced");
		}
		else
		{
			System.out.println("Not Balanced");
		}
	}
	private static boolean isValid(String st)
	{
		Stack<Character> sta = new Stack();
		boolean ans = true;
		int n;
		n = st.length();
		for(int i =0; i<n; i++)
		{
			if(st.charAt(i) == '[' || st.charAt(i) == '{' || st.charAt(i) == '(')
			{
				sta.push(st.charAt(i));
			}
			else
			{
				if(st.charAt(i) == ')')
				{
					if(!sta.empty() && sta.peek() == '(')
					{
						sta.pop();
					}
					else
					{
						ans = false;
						break;
					}
				}
				if(st.charAt(i) == '}')
				{
					if(!sta.empty() && sta.peek() == '{')
					{
						sta.pop();
					}
					else
					{
						ans = false;
						break;
					}
				}
				if(st.charAt(i) == ']')
				{
					if(!sta.empty() && sta.peek() == '[')
					{
						sta.pop();
					}
					else
					{
						ans = false;
						break;
					}
				}
			}
		}
		if(!sta.empty())
		{
			return false;
		}
		else
		{
			return ans;
		}
	}
}