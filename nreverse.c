#include<stdio.h>
int main()
{
	int n, temp, rem, sum;
	printf("Input: ");
	scanf("%d", &n);
	temp = n;
	sum = 0;
	while(temp)
	{
		rem = temp%10;
		sum = sum*10 + rem;
		temp = temp/10;
	}
	printf("Output: %d", sum);
	return 0;
}