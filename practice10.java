/*
Heapsort working process
step1: Satisfied three propertise such as build max-heap, largest items, and store in the root node
step2: swap: remove the root node and store at the end of array.
step3: Remove: reduce the size of list by 1
step4: Heapify the root element again for select higest/largest item in the root again
step5: Repeadly te prcess until we get the sorted list.

*/

import java.util.*;
class practice10{
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		System.out.print("Enter the length ");
		int n;
		n = input.nextInt();
		System.out.println("Enter the list? ");
		int[] arr;
		arr = new int[n];
		for(int i =0; i<n; i++)
		{
			arr[i] = input.nextInt();
		}
		heapSort(arr, n);
		System.out.println("Ascending order of given list ");
		for(int i =0; i<n; i++)
		{
			System.out.print(arr[i] + " ");
		}
	}
	private static void heapSort(int[] arr, int size)
	{
		/*
		Select the number of parent node
		*/
		for(int i = (size/2)-1; i>=0; i--)
		{
			heapify(arr, size, i);
		}

		/*
		heapsort the list
		*/
		int temp;
		for(int i = size - 1; i>=0; i--)
		{
			temp = arr[i];
			arr[i] = arr[0];
			arr[0] = temp;
			heapify(arr, i, 0);
		}
	}
	private static void heapify(int[] arr, int n, int index)
	{
		int i, l, r;
		i = index;
		l = 2*i + 1;
		r = 2*i + 2;

		/*
		check left child with root node
		*/
		if(l<n && arr[l]>arr[i])
		{
			i = l;
		}

		/*
		check right child with root node
		*/

		if(r<n && arr[r]> arr[i])
		{
			i = r;
		}

		/*
		set max items in the root node and heapify again the other node
		*/
		int temp;
		if( i != index)
		{
			temp = arr[i];
			arr[i] = arr[index];
			arr[index] = temp;
			heapify(arr, n, i);
		}
	}
}