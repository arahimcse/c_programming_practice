#include <stdio.h>
#include <stdlib.h>

int main()
{
    int a, b, c, d, e;
    printf("length of fibonacci series?  ");
    scanf("%d", &a);
    b = 0;
    c = 1;
    printf("%d\n%d\n", b, c);
    for(d = 2; d<a; d++)
    {
        e = b + c;
        b = c;
        c = e;
        printf("%d\n",e);
    }
    return 0;
}
