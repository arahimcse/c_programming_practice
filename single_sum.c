#include<stdio.h>
// Write a program that takes a positive interger as input and ind out the sumif the ditis repeatedly until result is converged to a single digit[BP-2018]

int main()
{
	int num, c, sum, rem;
	printf("Simple Input: ");
	scanf("%d", &num);
	c = 0;
	rem = 0;
	while(num/10 != 0)
	{
		sum = 0;
		c++;
		while(num != 0)
		{
			rem = num%10;
			sum += rem;
			num /=10;
		}
		num = sum;
		if(sum>10)
		{
			printf("Sum of digits: %d (%d Step) \n", sum, c);
		} else {
			printf("Sum of digits: %d (Final Step) \n", sum);
		}
	}
	return 0;
}