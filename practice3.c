/*
write a program that will sum the digits of a number until sum become a signle digits
example
input: 325221
Output: First step sum: 15
Final step sum: 6
*/

#include<stdio.h>

int main()
{
	int n;
	printf("Input: ");
	scanf("%d", &n);
	int sum, temp, rem, c;
	rem =0;
	c =0;
	while(n/10 != 0)
	{
		sum =0;
		c++;
		while(n)
		{
			rem = n%10;
			sum += rem;
			n /= 10;
		}
		n = sum;
		if (sum>9)
		{
			printf("Sum of digits = %d ( %d step) \n", sum, c);
		}
		else
		{
			printf("Sum of digits = %d ( Final Step)", sum);
		}

	}
	return 0;
}